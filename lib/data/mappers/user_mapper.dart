import 'package:mytasks/data/mappers/mapper.dart';
import 'package:mytasks/data/models/user_data.dart';
import 'package:mytasks/domain/entities/user.dart';

class UserMapper implements Mapper<User, UserData> {
  @override
  User deserialize(UserData data) {
    return User(
      id: data.id,
      login: data.login,
    );
  }

  @override
  UserData serialize(User data) {
    // do not need it right now
    throw UnimplementedError();
  }
}
