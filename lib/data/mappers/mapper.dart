abstract interface class Mapper<From, To> {
  To serialize(From data);

  From deserialize(To data);
}
