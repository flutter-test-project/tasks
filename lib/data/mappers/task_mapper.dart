import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mytasks/data/mappers/mapper.dart';
import 'package:mytasks/data/models/task_data.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/domain/entities/task_status.dart';

class TaskMapper implements Mapper<Task, TaskData> {
  @override
  Task deserialize(TaskData data) {
    return Task(
      id: data.id,
      title: data.title,
      description: data.description,
      creatorId: data.creatorId,
      creationData: DateTime.fromMillisecondsSinceEpoch(
          data.creationData.millisecondsSinceEpoch),
      status: TaskStatus.values.singleWhere((e) => e.id == data.statusId),
    );
  }

  @override
  TaskData serialize(Task data) {
    return TaskData(
      id: data.id,
      title: data.title,
      description: data.description,
      creatorId: data.creatorId,
      creationData: Timestamp.fromDate(data.creationData),
      statusId: data.status.id,
    );
  }
}
