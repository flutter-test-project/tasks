import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class TaskData extends Equatable {
  final String id;
  final String title;
  final String description;
  final String creatorId;
  final Timestamp creationData;
  final String statusId;

  const TaskData({
    required this.id,
    required this.title,
    required this.description,
    required this.creatorId,
    required this.creationData,
    required this.statusId,
  });

  factory TaskData.fromFirestore(
    QueryDocumentSnapshot<Map<String, dynamic>> snapshot,
  ) {
    final data = snapshot.data();
    return TaskData(
      id: snapshot.id,
      title: data[TaskDataFields.title],
      description: data[TaskDataFields.description],
      creatorId: data[TaskDataFields.creatorId],
      creationData: data[TaskDataFields.creationData],
      statusId: data[TaskDataFields.statusId],
    );
  }

  @override
  List<Object?> get props => [
        id,
        title,
        description,
        creatorId,
        creationData,
        statusId,
      ];
}

abstract final class TaskDataFields {
  static const creatorId = 'creatorId';
  static const description = 'description';
  static const title = 'title';
  static const statusId = 'statusId';
  static const creationData = 'creationData';
}
