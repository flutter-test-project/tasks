import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class UserData extends Equatable {
  final String id;
  final String login;
  final String password;

  const UserData({
    required this.id,
    required this.login,
    required this.password,
  });

  factory UserData.fromFirestore(
    QueryDocumentSnapshot<Map<String, dynamic>> snapshot,
  ) {
    final data = snapshot.data();
    return UserData(
      id: snapshot.id,
      login: data[UserDataFields.login],
      password: data[UserDataFields.password],
    );
  }

  @override
  List<Object?> get props => [id, login, password];
}

abstract final class UserDataFields {
  static const login = 'login';
  static const password = 'password';
}
