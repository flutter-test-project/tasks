import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mytasks/data/collections.dart';
import 'package:mytasks/data/mappers/task_mapper.dart';
import 'package:mytasks/data/models/task_data.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/domain/repository/tasks_repository.dart';

class TasksRepositoryFirestore implements TasksRepository {
  @override
  Future<List<Task>> getTasks({required String userId}) async {
    try {
      final List<Task> tasks = [];
      final db = FirebaseFirestore.instance;
      final QuerySnapshot querySnapshot = await db
          .collection(FirestoreCollections.tasks)
          .where(TaskDataFields.creatorId, isEqualTo: userId)
          .get();
      for (var doc in querySnapshot.docs) {
        final snapshot = doc as QueryDocumentSnapshot<Map<String, dynamic>>;
        final taskData = TaskData.fromFirestore(snapshot);
        tasks.add(TaskMapper().deserialize(taskData));
      }

      return tasks;
    } catch (error, stackTrace) {
      log(error.toString(), stackTrace: stackTrace);
      rethrow;
    }
  }

  @override
  Future<Task> createNewTask({
    required Task task,
  }) async {
    try {
      String taskId = '';
      final db = FirebaseFirestore.instance;
      final TaskData taskData = TaskMapper().serialize(task);
      await db.collection(FirestoreCollections.tasks).doc().set({
        TaskDataFields.title: taskData.title,
        TaskDataFields.description: taskData.description,
        TaskDataFields.creatorId: taskData.creatorId,
        TaskDataFields.statusId: taskData.statusId,
        TaskDataFields.creationData: taskData.creationData,
      });
      final QuerySnapshot querySnapshot = await db
          .collection(FirestoreCollections.tasks)
          .where(TaskDataFields.creatorId, isEqualTo: taskData.creatorId)
          .where(TaskDataFields.title, isEqualTo: taskData.title)
          .where(TaskDataFields.description, isEqualTo: taskData.description)
          .where(TaskDataFields.statusId, isEqualTo: taskData.statusId)
          .where(TaskDataFields.creationData, isEqualTo: taskData.creationData)
          .get();
      for (var doc in querySnapshot.docs) {
        taskId = doc.id;
      }
      return task.copyWith(id: taskId);
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }

  @override
  Future<void> deleteTask({
    required String taskId,
  }) async {
    try {
      final db = FirebaseFirestore.instance;
      await db.collection(FirestoreCollections.tasks).doc(taskId).delete();
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }

  @override
  Future<void> changeTaskStatus({
    required String taskId,
    required String statusId,
  }) async {
    try {
      final db = FirebaseFirestore.instance;
      await db
          .collection(FirestoreCollections.tasks)
          .doc(taskId)
          .update({TaskDataFields.statusId: statusId});
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }
}
