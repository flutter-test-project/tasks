import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mytasks/data/collections.dart';
import 'package:mytasks/data/mappers/user_mapper.dart';
import 'package:mytasks/data/models/user_data.dart';
import 'package:mytasks/data/storage_keys.dart';
import 'package:mytasks/domain/entities/user.dart';
import 'package:mytasks/domain/repository/user_repository.dart';

class SignInRepositoryFirestore implements UserRepository {
  @override
  Future<User?> signIn({
    required String login,
    required String password,
  }) async {
    try {
      final db = FirebaseFirestore.instance;
      final QuerySnapshot querySnapshot = await db
          .collection(FirestoreCollections.users)
          .where(UserDataFields.login, isEqualTo: login)
          .where(UserDataFields.password, isEqualTo: password)
          .get();
      //final List<QueryDocumentSnapshot> listDocumentSnapshots = querySnapshot.docs;
      for (var doc in querySnapshot.docs) {
        final QueryDocumentSnapshot<Map<String, dynamic>> snapshot =
            doc as QueryDocumentSnapshot<Map<String, dynamic>>;
        final taskData = UserData.fromFirestore(snapshot);
        const storage = FlutterSecureStorage();
        await storage.write(key: StorageKeys.userId, value: snapshot.id);
        return UserMapper().deserialize(taskData);
      }

      return null;
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }

  @override
  Future<String> getUserId() async {
    try {
      const storage = FlutterSecureStorage();
      String? value = await storage.read(key: StorageKeys.userId);
      return value ?? '';
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }
}
