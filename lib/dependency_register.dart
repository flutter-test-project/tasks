import 'package:get_it/get_it.dart';
import 'package:mytasks/domain/usecase/task_create_usecase.dart';
import 'package:mytasks/domain/usecase/task_delete_usecase.dart';

import 'data/repository_impl/firestore/sign_in_repository_firestore.dart';
import 'data/repository_impl/firestore/tasks_repository_firestore.dart';
import 'domain/repository/tasks_repository.dart';
import 'domain/repository/user_repository.dart';
import 'domain/usecase/get_tasks_usecase.dart';
import 'domain/usecase/get_user_usecase.dart';
import 'domain/usecase/task_change_status_usecase.dart';
import 'presentation/common_blocs/task_list/task_list_bloc.dart';

final getIt = GetIt.instance;

void registerDependencies() {
  //repository
  getIt.registerSingleton<UserRepository>(SignInRepositoryFirestore());
  getIt.registerSingleton<TasksRepository>(TasksRepositoryFirestore());

  //useCases
  getIt.registerSingleton(
    GetUserUseCase(
      signInRepository: getIt(),
    ),
  );
  getIt.registerSingleton(
    GetTasksUseCase(
      userRepository: getIt(),
      taskRepository: getIt(),
    ),
  );
  getIt.registerSingleton(
    TaskCreateUseCase(
      tasksRepository: getIt(),
      userRepository: getIt(),
    ),
  );
  getIt.registerSingleton(
    TaskDeleteUseCase(
      tasksRepository: getIt(),
    ),
  );
  getIt.registerSingleton(
    TaskChangeStatusUseCase(
      tasksRepository: getIt(),
    ),
  );

  //bloc
  getIt.registerSingleton(TaskListBloc(getIt()));
}
