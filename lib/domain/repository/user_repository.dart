import '../entities/user.dart';

abstract interface class UserRepository {
  Future<User?> signIn({required String login, required String password});

  Future<String> getUserId();
}
