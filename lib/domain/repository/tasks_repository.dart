import '../entities/task.dart';

abstract interface class TasksRepository {
  Future<List<Task>> getTasks({required String userId});

  Future<Task> createNewTask({
    required Task task,
  });

  Future<void> deleteTask({
    required String taskId,
  });

  Future<void> changeTaskStatus({
    required String taskId,
    required String statusId,
  });
}
