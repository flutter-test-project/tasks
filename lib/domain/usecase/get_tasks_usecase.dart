import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/domain/repository/tasks_repository.dart';
import 'package:mytasks/domain/repository/user_repository.dart';
import 'base_usecase.dart';

class GetTasksUseCase extends BaseUseCase<Future<List<Task>>, NoParams> {
  final UserRepository _userRepository;
  final TasksRepository _tasksRepository;

  GetTasksUseCase({
    required UserRepository userRepository,
    required TasksRepository taskRepository,
  })  : _userRepository = userRepository,
        _tasksRepository = taskRepository;

  @override
  Future<List<Task>> call(NoParams params) async {
    final String userId = await _userRepository.getUserId();
    final List<Task> tasks = await _tasksRepository.getTasks(userId: userId);
    return tasks;
  }
}
