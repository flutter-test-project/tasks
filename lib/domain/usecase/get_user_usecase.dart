import 'package:mytasks/domain/repository/user_repository.dart';
import 'package:mytasks/domain/usecase/base_usecase.dart';

import 'package:mytasks/domain/entities/user.dart';

class GetUserUseCase extends BaseUseCase<Future<User?>, SignInData> {
  final UserRepository _userRepository;

  GetUserUseCase({required UserRepository signInRepository})
      : _userRepository = signInRepository;

  @override
  Future<User?> call(SignInData params) async {
    final User? result = await _userRepository.signIn(
      login: params.login,
      password: params.password,
    );
    return result;
  }
}

class SignInData {
  final String login;
  final String password;

  SignInData(this.login, this.password);
}
