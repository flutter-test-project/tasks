import 'package:mytasks/domain/repository/tasks_repository.dart';
import 'package:mytasks/domain/usecase/base_usecase.dart';

class TaskChangeStatusUseCase
    extends BaseUseCase<Future<void>, TaskChangeStatusData> {
  final TasksRepository _tasksRepository;

  TaskChangeStatusUseCase({required TasksRepository tasksRepository})
      : _tasksRepository = tasksRepository;

  @override
  Future<void> call(TaskChangeStatusData params) async {
     await _tasksRepository.changeTaskStatus(
      taskId: params.taskId,
      statusId: params.statusId,
    );
  }
}

class TaskChangeStatusData {
  final String taskId;
  final String statusId;

  TaskChangeStatusData({
    required this.taskId,
    required this.statusId,
  });
}
