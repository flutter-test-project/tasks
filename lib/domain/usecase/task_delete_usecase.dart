import 'package:mytasks/domain/repository/tasks_repository.dart';
import 'base_usecase.dart';

class TaskDeleteUseCase extends BaseUseCase<Future<void>, String> {
  final TasksRepository _tasksRepository;

  TaskDeleteUseCase({
    required TasksRepository tasksRepository,
  })  : _tasksRepository = tasksRepository;

  @override
  Future<void> call(String params) async {
    await _tasksRepository.deleteTask(
      taskId: params,
    );
  }
}
