import 'package:mytasks/domain/repository/user_repository.dart';

import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/domain/repository/tasks_repository.dart';
import 'base_usecase.dart';

class TaskCreateUseCase
    extends BaseUseCase<Future<Task>, CreateNewTaskData> {
  final TasksRepository _tasksRepository;
  final UserRepository _userRepository;

  TaskCreateUseCase({
    required TasksRepository tasksRepository,
    required UserRepository userRepository,
  })  : _tasksRepository = tasksRepository,
        _userRepository = userRepository;

  @override
  Future<Task> call(CreateNewTaskData params) async {
    final creatorId = await _userRepository.getUserId();
    final Task task = Task(
      title: params.title,
      description: params.description,
      status: params.taskStatus,
      creationData: DateTime.now(),
      creatorId: creatorId,
      id: '',
    );
    final Task newTask = await _tasksRepository.createNewTask(task: task);
    return newTask;
  }
}

class CreateNewTaskData {
  final String title;
  final String description;
  final TaskStatus taskStatus;

  CreateNewTaskData({
    required this.title,
    required this.description,
    required this.taskStatus,
  });
}
