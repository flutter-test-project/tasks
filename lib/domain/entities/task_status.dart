import 'package:flutter/material.dart';
import 'package:mytasks/presentation/utils/styles.dart';

// TODO: create presentation adapters for colors and strings
enum TaskStatus {
  todo(
    id: 'yYDWVoP3qnYbhactFdAK',
    name: 'Новая',
    allowed: [TaskStatus.inProgress],
    isDelete: true,
    color: redColor,
  ),
  inProgress(
    id: '406fykb1SZbZTtokYy3F',
    name: 'В работе',
    allowed: [TaskStatus.done],
    isDelete: false,
    color: blueColor,
  ),
  done(
    id: '8T89qaUQWO2hypYFh59c',
    name: 'Выполнена',
    allowed: [],
    isDelete: true,
    color: greenColor,
  );

  final String id;
  final String name;
  final List<TaskStatus> allowed;
  final bool isDelete;
  final Color color;

  const TaskStatus({
    required this.id,
    required this.name,
    required this.allowed,
    required this.isDelete,
    this.color = lightGrayColor,
  });
}
