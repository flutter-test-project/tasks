import 'package:equatable/equatable.dart';

import 'task_status.dart';

class Task extends Equatable {
  final String id;
  final String title;
  final String description;
  final String creatorId;
  final DateTime creationData;
  final TaskStatus status;

  const Task({
    required this.id,
    required this.title,
    required this.description,
    required this.creatorId,
    required this.creationData,
    required this.status,
  });

  Task copyWith({
    String? id,
    String? title,
    String? description,
    String? creatorId,
    DateTime? creationData,
    TaskStatus? status,
  }) {
    return Task(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      creatorId: creatorId ?? this.creatorId,
      creationData: creationData ?? this.creationData,
      status: status ?? this.status,
    );
  }

  @override
  List<Object?> get props => [
        id,
        title,
        description,
        creatorId,
        creationData,
        status,
      ];
}
