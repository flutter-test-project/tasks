import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String id;
  final String login;

  const User({
    required this.id,
    required this.login,
  });

  @override
  List<Object?> get props => [id, login];
}
