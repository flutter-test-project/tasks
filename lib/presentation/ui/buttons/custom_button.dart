import 'package:flutter/material.dart';

import '../../utils/styles.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;

  const CustomButton({super.key, required this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.bottomCenter,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        color: blueColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: const BorderSide(color: whiteColor),
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 16.0,
        ),
        onPressed: onPressed,
        child: Text(
          text,
          style: headingStyle.copyWith(color: whiteColor),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}