import 'package:flutter/material.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () => Navigator.pop(context),
      padding: const EdgeInsets.all(4),
      shape: const CircleBorder(),
      child: const Icon(
        Icons.keyboard_backspace_outlined,
        size: 24,
      ),
    );
  }
}
