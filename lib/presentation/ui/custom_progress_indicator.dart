import 'package:flutter/cupertino.dart';

class CustomProgressIndicator extends StatelessWidget {
  const CustomProgressIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CupertinoActivityIndicator(
          radius: 14,
        ),
      ),
    );
  }
}
