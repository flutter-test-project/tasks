import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utils/styles.dart';

void optionDialog({
  required BuildContext context,
  required String text,
  Function()? onYesPressed,
  Function()? onNoPressed,
}) {
  showDialog<String>(
    context: context,
    builder: (BuildContext context) => OptionDialog(
      text: text,
      onYesPressed: onYesPressed,
      onNoPressed: onNoPressed,
    ),
  );
}

class OptionDialog extends StatelessWidget {
  final String text;
  final void Function()? onYesPressed;
  final void Function()? onNoPressed;

  const OptionDialog({
    super.key,
    required this.text,
    required this.onYesPressed,
    required this.onNoPressed,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 8),
          Icon(
            CupertinoIcons.question,
            size: 60,
            color: Colors.grey[400],
          ),
          const SizedBox(height: 8),
          Text(text, style: normalStyle),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: onYesPressed ?? () => Navigator.pop(context),
          child: const Text('Да', style: headingStyle),
        ),
        TextButton(
          onPressed: onNoPressed ?? () => Navigator.pop(context),
          child: const Text('Нет', style: headingStyle),
        ),
      ],
    );
  }
}
