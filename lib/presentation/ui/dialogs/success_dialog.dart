import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mytasks/presentation/utils/styles.dart';

void successDialog({
  required BuildContext context,
  required String text,
  Function()? onPressed,
}) {
  showDialog<String>(
    context: context,
    builder: (BuildContext context) => SuccessDialog(
      text: text,
      onPressed: onPressed,
    ),
  );
}

class SuccessDialog extends StatelessWidget {
  final String text;
  final Function()? onPressed;

  const SuccessDialog({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 8),
          Icon(
            CupertinoIcons.exclamationmark,
            size: 60,
            color: Colors.grey[400],
          ),
          const SizedBox(height: 8),
          Text(text, style: headingStyle),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: onPressed ?? () => Navigator.pop(context),
          child: const Text('OK'),
        ),
      ],
    );
  }
}
