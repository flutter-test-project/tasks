import 'package:flutter/material.dart';

import '../../utils/styles.dart';
import '../buttons/custom_back_button.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String text;
  final bool isShowBackButton;
  final List<Widget> actions;

  const MainAppBar({
    super.key,
    required this.text,
    this.isShowBackButton = true,
    this.actions = const [],
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: isShowBackButton ? const CustomBackButton() : null,
      title: Text(
        text.toUpperCase(),
        style: appBarTextStyle,
      ),
      actions: actions,
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
