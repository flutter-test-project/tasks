import 'package:flutter/material.dart';

//Color
const Color backgroundColor = Color.fromRGBO(255, 255, 255, 1);
const Color whiteColor = Color.fromRGBO(255, 255, 255, 1);
const Color redColor = Color.fromRGBO(244, 67, 54, 1);
const Color greenColor = Color.fromRGBO(63, 145, 109, 1);
const Color blueColor = Color.fromRGBO(32, 74, 181, 1);
const Color lightGrayColor = Color.fromRGBO(153, 153, 153, 1);
const Color paleGrayColor = Color.fromRGBO(225, 234, 243, 1.0);
const Color grayColor = Color.fromRGBO(109, 109, 118, 1);
const Color blackColor = Color.fromRGBO(0, 0, 0, 1);

//Style
const TextStyle appBarTextStyle = TextStyle(
  color: blackColor,
  fontWeight: FontWeight.bold,
  fontSize: 18,
  height: 24 / 18,
);

const TextStyle headerStyle = TextStyle(
  color: blackColor,
  fontWeight: FontWeight.bold,
  fontSize: 20,
);

const headingStyle = TextStyle(
  color: blackColor,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);

const TextStyle normalStyle = TextStyle(
  color: blackColor,
  fontSize: 14,
);
