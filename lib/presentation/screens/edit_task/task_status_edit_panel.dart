import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/presentation/common_blocs/task_edit/task_edit_bloc.dart';
import 'package:mytasks/presentation/common_blocs/task_list/task_list_bloc.dart';
import 'package:mytasks/presentation/ui/dialogs/error_dialog.dart';

import 'widgets/task_status_change_dropdown_button.dart';

class TaskStatusEditPanel extends StatelessWidget {
  final String idTask;
  final TaskStatus taskStatus;

  const TaskStatusEditPanel({
    super.key,
    required this.taskStatus,
    required this.idTask,
  });

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskEditBloc, TaskEditState>(
      listener: (context, state) {
        if (state is EditStatusErrorState) {
          showErrorDialog(
            context: context,
            errorText:
                'Не получилось изменить статус задачи. Попробуйте повторить попытку позже.',
            text: 'Ошибка',
          );
        } else if (state is EditStatusSuccessState) {
          _changeTaskStatusInList(
            context,
            taskId: state.taskId,
            taskStatus: state.taskStatus,
          );
        }
      },
      builder: (context, state) {
        return TaskStatusChangeDropdownButton(
          value: taskStatus,
          color: taskStatus.color,
          isExpanded: false,
          onChanged: state is TaskEditLoadingState
              ? null
              : (TaskStatus? newStatus) {
                  if (newStatus != null) {
                    BlocProvider.of<TaskEditBloc>(context).add(
                      TaskEditStatusEvent(
                        newStatus: newStatus,
                        taskId: idTask,
                      ),
                    );
                  }
                },
        );
      },
    );
  }

  void _changeTaskStatusInList(
    BuildContext context, {
    required String taskId,
    required TaskStatus taskStatus,
  }) {
    BlocProvider.of<TaskListBloc>(context).add(
      ChangeTaskStatusEvent(
        taskId: taskId,
        taskStatus: taskStatus,
      ),
    );
  }
}
