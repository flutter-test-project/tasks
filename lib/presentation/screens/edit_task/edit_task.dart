import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/presentation/common_blocs/task_delete/task_delete_bloc.dart';
import 'package:mytasks/presentation/common_blocs/task_edit/task_edit_bloc.dart';
import 'package:mytasks/presentation/common_blocs/task_list/task_list_bloc.dart';
import 'package:mytasks/presentation/screens/home/home_screen.dart';
import 'package:mytasks/presentation/screens/home/widgets/delete_task_icon_button.dart';
import 'package:mytasks/presentation/ui/app_bar/main_app_bar.dart';
import 'package:mytasks/presentation/ui/custom_progress_indicator.dart';
import 'package:mytasks/presentation/ui/dialogs/error_dialog.dart';
import 'package:mytasks/presentation/ui/dialogs/success_dialog.dart';
import 'package:mytasks/presentation/utils/styles.dart';

import 'task_status_edit_panel.dart';

class EditTask extends StatelessWidget {
  final String taskId;

  const EditTask({super.key, required this.taskId});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<TaskEditBloc>(
          create: (BuildContext context) => TaskEditBloc(GetIt.instance()),
        ),
        BlocProvider<TaskDeleteBloc>(
          create: (BuildContext context) => TaskDeleteBloc(GetIt.instance()),
        ),
      ],
      child: EditTaskPanel(taskId: taskId),
    );
  }
}

class EditTaskPanel extends StatelessWidget {
  final String taskId;

  const EditTaskPanel({super.key, required this.taskId});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskListBloc, TaskListState>(
      builder: (context, taskListState) {
        Task? task;
        if (taskListState is TaskListLoadedState) {
          task = taskListState.getTask(taskId);
        }

        return Scaffold(
          appBar: MainAppBar(
            text: 'Редактирование задачи',
            actions: [
              if (task != null)
                DeleteTaskIconButton(
                  taskId: task.id,
                  isDelete: task.status.isDelete,
                ),
            ],
          ),
          body: BlocListener<TaskDeleteBloc, TaskDeleteState>(
            listener: (context, state) {
              if (state is TaskDeleteErrorState) {
                showErrorDialog(
                  context: context,
                  errorText: 'Не удалось удалить задачу. Попробуйте повторить операцию позже.',
                  text: 'Ошибка',
                );
              } else if (state is TaskDeleteSuccessState) {
                _deleteTask(context, taskId: state.taskId);
                successDialog(
                  context: context,
                  text: 'Задача была успешно удалена',
                  onPressed: () {
                    Navigator.pop(context);
                    _goToTaskList(context);
                  },
                );
              }
            },
            child: switch (taskListState) {
              TaskListLoadingState _ => const CustomProgressIndicator(),
              TaskListLoadedState _ =>
                task != null ? _EditTaskContentPanel(task: task) : _NoTask(),
              _ => const SizedBox(),
            },
          ),
        );
      },
    );
  }

  void _deleteTask(BuildContext context, {required String taskId}) {
    BlocProvider.of<TaskListBloc>(context).add(DeleteTaskEvent(taskId));
  }

  void _goToTaskList(BuildContext context) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => const HomeScreen(),
      ),
    );
  }
}

class _NoTask extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text(
        'Такой задачи нет',
        style: normalStyle,
      ),
    );
  }
}

class _EditTaskContentPanel extends StatelessWidget {
  final Task task;

  const _EditTaskContentPanel({required this.task});

  @override
  Widget build(BuildContext context) {
    final String data = DateFormat('dd.mm.yyyy H:mm').format(task.creationData);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Text(
              task.title,
              style: headerStyle,
            ),
          ),
          const SizedBox(height: 6),
          Text(
            task.description,
            style: normalStyle,
          ),
          const SizedBox(height: 6),
          Text('От: $data'),
          const SizedBox(height: 6),
          TaskStatusEditPanel(
            idTask: task.id,
            taskStatus: task.status,
          ),
        ],
      ),
    );
  }
}
