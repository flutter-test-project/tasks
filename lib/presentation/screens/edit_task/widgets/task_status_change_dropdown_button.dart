import 'package:flutter/material.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/presentation/utils/styles.dart';

class TaskStatusChangeDropdownButton extends StatelessWidget {
  final TaskStatus value;
  final Color color;
  final Color iconEnabledColor;
  final TextStyle? textStyle;
  final void Function(TaskStatus?)? onChanged;
  final bool isExpanded;

  const TaskStatusChangeDropdownButton({
    super.key,
    required this.value,
    this.color = paleGrayColor,
    this.iconEnabledColor = whiteColor,
    this.onChanged,
    this.textStyle,
    this.isExpanded = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: color,
      ),
      child: DropdownButton<TaskStatus>(
        isExpanded: isExpanded,
        elevation: 5,
        dropdownColor: color,
        iconEnabledColor: iconEnabledColor,
        value: value,
        style: textStyle,
        onChanged: onChanged,
        items: [value, ...value.allowed]
            .map(
              (TaskStatus item) => DropdownMenuItem(
                value: item,
                child: Text(
                  item.name,
                  style: textStyle ??
                      normalStyle.copyWith(
                        color: whiteColor,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            )
            .toList(),
        padding: const EdgeInsets.only(left: 16, right: 6),
        borderRadius: BorderRadius.circular(8),
        underline: const SizedBox(),
      ),
    );
  }
}
