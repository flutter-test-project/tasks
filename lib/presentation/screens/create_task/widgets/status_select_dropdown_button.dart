import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/presentation/utils/styles.dart';

import '../bloc/create_task_bloc.dart';

class StatusSelectDropdownButton extends StatelessWidget {
  const StatusSelectDropdownButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateTaskBloc, CreateTaskState>(
      builder: (context, state) {
        if (state is CreateTaskLoadedState) {
          return DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: paleGrayColor,
            ),
            child: DropdownButton<TaskStatus>(
              isExpanded: true,
              elevation: 5,
              dropdownColor: paleGrayColor,
              focusColor: lightGrayColor,
              value: state.taskStatus,
              onChanged: (TaskStatus? taskStatus) =>
                  _changeStatus(context, taskStatus),
              items: TaskStatus.values
                  .map(
                    (TaskStatus item) => DropdownMenuItem(
                      value: item,
                      child: Text(
                        item.name,
                        style: normalStyle,
                      ),
                    ),
                  )
                  .toList(),
              padding: const EdgeInsets.only(left: 16, right: 6),
              underline: const SizedBox(),
            ),
          );
        }
        return const SizedBox();
      },
    );
  }

  void _changeStatus(BuildContext context, TaskStatus? taskStatus) {
    if (taskStatus != null) {
      BlocProvider.of<CreateTaskBloc>(context)
          .add(ChangeSelectStatusTask(taskStatus));
    }
  }
}
