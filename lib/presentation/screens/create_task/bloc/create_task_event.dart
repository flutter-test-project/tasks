part of 'create_task_bloc.dart';

@immutable
abstract class CreateTaskEvent extends Equatable {
  const CreateTaskEvent();

  @override
  List<Object?> get props => [];
}

class AddNewTaskEvent extends CreateTaskEvent {
  final String title;
  final String description;

  const AddNewTaskEvent({
    required this.title,
    required this.description,
  });

  @override
  List<Object?> get props => [title, description];
}

class ChangeSelectStatusTask extends CreateTaskEvent {
  final TaskStatus taskStatus;

  const ChangeSelectStatusTask(this.taskStatus);

  @override
  List<Object?> get props => [taskStatus];
}
