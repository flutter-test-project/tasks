import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/domain/usecase/task_create_usecase.dart';

part 'create_task_event.dart';

part 'create_task_state.dart';

class CreateTaskBloc extends Bloc<CreateTaskEvent, CreateTaskState> {
  final TaskCreateUseCase _taskCreateUseCase;

  CreateTaskBloc(this._taskCreateUseCase)
      : super(const CreateTaskLoadedState()) {
    on<AddNewTaskEvent>(_addTask);
    on<ChangeSelectStatusTask>(_changeStatus);
  }

  Future<void> _changeStatus(
      ChangeSelectStatusTask event, Emitter<CreateTaskState> emit) async {
    emit(CreateTaskLoadedState(taskStatus: event.taskStatus));
  }

  Future<void> _addTask(
      AddNewTaskEvent event, Emitter<CreateTaskState> emit) async {
    final oldState = state;
    if (oldState is CreateTaskLoadedState) {
      try {
        emit(oldState.copyWith(isLoading: true));
        final Task newTask = await _taskCreateUseCase.call(CreateNewTaskData(
          title: event.title,
          description: event.description,
          taskStatus: oldState.taskStatus,
        ),);
        if (newTask.id.isNotEmpty) {
          emit(CreateTaskSuccessState(
            successText: 'Новая задача создана',
            newTask: newTask,
          ));
        } else {
          emit(const CreateTaskErrorState(
              'Не удалось создать новую задачу. Попробуйте повторить попытку позже.'));
        }
        emit(const CreateTaskLoadedState());
      } catch (error) {
        emit(CreateTaskErrorState(error.toString()));
        emit(const CreateTaskLoadedState());
      }
    }
  }
}
