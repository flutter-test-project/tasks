part of 'create_task_bloc.dart';

@immutable
sealed class CreateTaskState extends Equatable {
  const CreateTaskState();

  @override
  List<Object?> get props => [];
}

class CreateTaskInitState extends CreateTaskState {}

class CreateTaskLoadingState extends CreateTaskState {}

class CreateTaskLoadedState extends CreateTaskState {
  final TaskStatus taskStatus;
  final bool isLoading;

  const CreateTaskLoadedState({
    this.taskStatus = TaskStatus.todo,
    this.isLoading = false,
  });

  CreateTaskLoadedState copyWith({
    TaskStatus? taskStatus,
    bool? isLoading,
  }) {
    return CreateTaskLoadedState(
      taskStatus: taskStatus ?? this.taskStatus,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object?> get props => [taskStatus, isLoading];
}

class CreateTaskErrorState extends CreateTaskState {
  final String errorText;

  const CreateTaskErrorState(this.errorText);
}

class CreateTaskSuccessState extends CreateTaskState {
  final Task newTask;
  final String successText;

  const CreateTaskSuccessState({
    required this.successText,
    required this.newTask,
  });
}
