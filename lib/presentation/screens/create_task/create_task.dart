import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/presentation/common_blocs/task_list/task_list_bloc.dart';
import 'package:mytasks/presentation/screens/edit_task/edit_task.dart';
import 'package:mytasks/presentation/ui/app_bar/main_app_bar.dart';
import 'package:mytasks/presentation/ui/buttons/custom_button.dart';
import 'package:mytasks/presentation/ui/custom_progress_indicator.dart';
import 'package:mytasks/presentation/ui/custom_text_panel.dart';
import 'package:mytasks/presentation/ui/dialogs/error_dialog.dart';
import 'package:mytasks/presentation/ui/dialogs/option_dialog.dart';
import 'package:mytasks/presentation/utils/styles.dart';
import 'package:mytasks/presentation/utils/validators.dart';

import 'bloc/create_task_bloc.dart';
import 'widgets/status_select_dropdown_button.dart';

class CreateTask extends StatelessWidget {
  const CreateTask({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => CreateTaskBloc(GetIt.instance()),
      child: _CreateTaskPage(),
    );
  }
}

class _CreateTaskPage extends StatefulWidget {
  @override
  State<_CreateTaskPage> createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<_CreateTaskPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(
        text: 'Создать новую задачу',
      ),
      body: BlocConsumer<CreateTaskBloc, CreateTaskState>(
        listener: (context, state) {
          if (state is CreateTaskErrorState) {
            showErrorDialog(
              context: context,
              errorText: state.errorText,
              text: 'Ошибка',
            );
          } else if (state is CreateTaskSuccessState) {
            optionDialog(
              context: context,
              text: '${state.successText}. Хотите перейти к ее редактированию?',
              onYesPressed: () =>
                  _goToEditTask(context, taskId: state.newTask.id),
            );
          }
        },
        builder: (context, state) {
          return switch (state) {
            CreateTaskLoadingState _ => const CustomProgressIndicator(),
            CreateTaskLoadedState _ => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: _CreatedTaskForm(isLoading: state.isLoading),
              ),
            _ => const SizedBox(),
          };
        },
      ),
    );
  }

  void _goToEditTask(BuildContext context, {required String taskId}) {
    // to close dialog
    Navigator.pop(context);
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => EditTask(
        taskId: taskId,
      ),
    ));
  }
}

class _CreatedTaskForm extends StatefulWidget {
  final bool isLoading;

  const _CreatedTaskForm({required this.isLoading});

  @override
  State<_CreatedTaskForm> createState() => _CreatedTaskFormState();
}

class _CreatedTaskFormState extends State<_CreatedTaskForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();

  final TextEditingController _descriptionController = TextEditingController();

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CreateTaskBloc, CreateTaskState>(
      listener: (context, state) {
        if (state is CreateTaskSuccessState) {
          _addTaskInList(context, newTask: state.newTask);
          _titleController.clear();
          _descriptionController.clear();
        }
      },
      child: Form(
        key: _formKey,
        child: ListView(
          children: [
            const Text(
              'Название задачи',
              style: headingStyle,
            ),
            const SizedBox(height: 8),
            CustomTextPanel(
              controller: _titleController,
              hintText: 'Введите название задачи',
              validator: notEmptyTextFieldValidator,
            ),
            const SizedBox(height: 8),
            const Text(
              'Описание задачи',
              style: headingStyle,
            ),
            const SizedBox(height: 8),
            CustomTextPanel(
              controller: _descriptionController,
              hintText: 'Введите описание задачи',
              validator: notEmptyTextFieldValidator,
              minLines: 5,
              maxLines: 50,
            ),
            const SizedBox(height: 8),
            const Text(
              'Выберите статус задачи',
              style: headingStyle,
            ),
            const SizedBox(height: 8),
            const StatusSelectDropdownButton(),
            const SizedBox(height: 8),
            if (!widget.isLoading)
              CustomButton(
                text: 'Создать задачу',
                onPressed: () => _createTask(context),
              ),
            if (widget.isLoading) const CustomProgressIndicator(),
          ],
        ),
      ),
    );
  }

  void _createTask(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      BlocProvider.of<CreateTaskBloc>(context).add(
        AddNewTaskEvent(
          title: _titleController.text,
          description: _descriptionController.text,
        ),
      );
    }
  }

  void _addTaskInList(BuildContext context, {required Task newTask}) {
    BlocProvider.of<TaskListBloc>(context).add(TaskListRefreshEvent());
  }
}
