import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/presentation/common_blocs/task_delete/task_delete_bloc.dart';
import 'package:mytasks/presentation/ui/dialogs/option_dialog.dart';
import 'package:mytasks/presentation/utils/styles.dart';

class DeleteTaskIconButton extends StatelessWidget {
  final bool isDelete;
  final String taskId;

  const DeleteTaskIconButton({
    super.key,
    required this.isDelete,
    required this.taskId,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed:
          isDelete ? () => _onOptionDeleteTask(context, taskId: taskId) : null,
      padding: EdgeInsets.zero,
      icon: Icon(
        CupertinoIcons.trash,
        color: isDelete ? redColor : lightGrayColor,
        size: 20,
      ),
    );
  }

  void _onOptionDeleteTask(BuildContext context, {required String taskId}) {
    optionDialog(
      context: context,
      text: 'Вы уверены что хотите удалить задачу?',
      onYesPressed: () => _deleteTask(context, taskId: taskId),
    );
  }

  void _deleteTask(BuildContext context, {required String taskId}) {
    Navigator.pop(context);
    BlocProvider.of<TaskDeleteBloc>(context).add(
      DeleteEvent(taskId),
    );
  }
}
