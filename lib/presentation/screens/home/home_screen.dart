import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mytasks/presentation/common_blocs/task_delete/task_delete_bloc.dart';
import 'package:mytasks/presentation/common_blocs/task_edit/task_edit_bloc.dart';
import 'package:mytasks/presentation/common_blocs/task_list/task_list_bloc.dart';
import "package:mytasks/presentation/screens/create_task/create_task.dart";
import 'package:mytasks/presentation/screens/home/task_panel.dart';
import 'package:mytasks/presentation/ui/app_bar/main_app_bar.dart';
import 'package:mytasks/presentation/ui/custom_progress_indicator.dart';
import 'package:mytasks/presentation/ui/dialogs/error_dialog.dart';
import 'package:mytasks/presentation/ui/dialogs/success_dialog.dart';
import 'package:mytasks/presentation/utils/styles.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<TaskEditBloc>(
          create: (BuildContext context) => TaskEditBloc(GetIt.instance()),
        ),
        BlocProvider<TaskDeleteBloc>(
          create: (BuildContext context) => TaskDeleteBloc(GetIt.instance()),
        ),
      ],
      child: const _TasksPage(),
    );
  }
}

class _TasksPage extends StatefulWidget {
  const _TasksPage();

  @override
  State<_TasksPage> createState() => _TasksPageState();
}

class _TasksPageState extends State<_TasksPage> {
  @override
  void initState() {
    BlocProvider.of<TaskListBloc>(context).add(TaskListFetchedEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(
        text: 'Задачи',
        isShowBackButton: false,
        actions: [
          AddTaskButton(),
        ],
      ),
      body: BlocListener<TaskDeleteBloc, TaskDeleteState>(
        listener: (context, state) {
          if (state is TaskDeleteErrorState) {
            showErrorDialog(
              context: context,
              errorText:
                  'Не удалось удалить задачу. Попробуйте повторить операцию позже.',
              text: 'Ошибка',
            );
          } else if (state is TaskDeleteSuccessState) {
            successDialog(
              context: context,
              text: 'Задача была успешно удалена',
            );
            _deleteTask(context, taskId: state.taskId);
          }
        },
        child: BlocConsumer<TaskListBloc, TaskListState>(
          listener: (context, state) {
            if (state is TaskListErrorState) {
              showErrorDialog(
                context: context,
                errorText: state.errorText,
                text: 'Ошибка',
              );
            }
          },
          builder: (context, state) {
            if (state is TaskListLoadingState) {
              return const CustomProgressIndicator();
            } else if (state is TaskListLoadedState) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: _TasksPanel(),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }

  void _deleteTask(BuildContext context, {required String taskId}) {
    BlocProvider.of<TaskListBloc>(context).add(DeleteTaskEvent(taskId));
  }
}

class _TasksPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskListBloc, TaskListState>(
      builder: (context, state) {
        if (state is TaskListLoadedState) {
          return RefreshIndicator(
            onRefresh: () => _refresh(context),
            child: ListView(
              children: [
                Text(
                  'Новых задач: ${state.countTaskStatusToDo}',
                  style: headingStyle,
                ),
                const SizedBox(height: 6),
                ...state.tasksDataSort
                    .map(
                      (e) => Padding(
                        padding: const EdgeInsets.only(bottom: 6),
                        child: TaskPanel(e),
                      ),
                    )
                    .toList(),
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }

  _refresh(BuildContext context) =>
      BlocProvider.of<TaskListBloc>(context).add(TaskListRefreshEvent());
}

class AddTaskButton extends StatelessWidget {
  const AddTaskButton({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => onPressed(context),
      icon: const Icon(
        Icons.add,
        size: 24,
      ),
    );
  }

  void onPressed(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => const CreateTask(),
    ));
  }
}
