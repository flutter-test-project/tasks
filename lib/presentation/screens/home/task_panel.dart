import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/presentation/screens/edit_task/edit_task.dart';
import 'package:mytasks/presentation/screens/edit_task/task_status_edit_panel.dart';
import 'package:mytasks/presentation/utils/styles.dart';

import 'widgets/delete_task_icon_button.dart';

class TaskPanel extends StatelessWidget {
  final Task task;

  const TaskPanel(this.task, {super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      key: ValueKey(task.id),
      onTap: () => _goToEditTask(context, taskId: task.id),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: task.status.color),
          borderRadius: const BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(width: 8),
            Expanded(
              child: _TaskInfoPanel(task),
            ),
            const SizedBox(width: 4),
            DeleteTaskIconButton(
              taskId: task.id,
              isDelete: task.status.isDelete,
            ),
          ],
        ),
      ),
    );
  }

  void _goToEditTask(BuildContext context, {required String taskId}) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => EditTask(taskId: taskId),
      ),
    );
  }
}

class _TaskInfoPanel extends StatelessWidget {
  final Task task;

  const _TaskInfoPanel(this.task);

  @override
  Widget build(BuildContext context) {
    final String data = DateFormat('dd.mm.yyyy H:mm').format(task.creationData);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 6),
        Text(
          task.title,
          style: headingStyle,
        ),
        const SizedBox(height: 2),
        Text(
          data,
          style: normalStyle,
        ),
        const SizedBox(height: 6),
        TaskStatusEditPanel(
          idTask: task.id,
          taskStatus: task.status,
        ),
        const SizedBox(height: 6),
      ],
    );
  }
}
