import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mytasks/presentation/screens/home/home_screen.dart';
import 'package:mytasks/presentation/ui/custom_text_panel.dart';

import '../../ui/buttons/custom_button.dart';
import '../../ui/custom_progress_indicator.dart';
import '../../ui/dialogs/error_dialog.dart';
import '../../utils/styles.dart';
import '../../utils/validators.dart';
import 'bloc/sign_in_bloc.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => SignInBloc(GetIt.instance()),
      child: _SignInPage(),
    );
  }
}

class _SignInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<SignInBloc, SignInState>(
        listener: (context, state) {
          if (state is SignInErrorState) {
            showErrorDialog(
              context: context,
              errorText: state.errorText,
              text: 'Ошибка',
            );
          } else if (state is SignInLoadedState) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => const HomeScreen(),
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is SignInLoadingState) {
            return const CustomProgressIndicator();
          } else {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: _SignInForm(),
            );
          }
        },
      ),
    );
  }
}

class _SignInForm extends StatefulWidget {
  @override
  State<_SignInForm> createState() => _SignInFormState();
}

class _SignInFormState extends State<_SignInForm> {
  bool _visibility = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _signInController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _signInController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Авторизация',
            style: headingStyle,
          ),
          const SizedBox(height: 8),
          CustomTextPanel(
            controller: _signInController,
            hintText: 'Логин',
            validator: notEmptyTextFieldValidator,
          ),
          const SizedBox(height: 8),
          CustomTextPanel(
            controller: _passwordController,
            hintText: 'Пароль',
            validator: notEmptyTextFieldValidator,
            obscureText: !_visibility,
            suffixIcon: IconButton(
              onPressed: _onPasswordVisibility,
              icon: Icon(
                _visibility ? Icons.visibility : Icons.visibility_off,
                color: Colors.black,
              ),
            ),
          ),
          const SizedBox(height: 8),
          CustomButton(
            text: 'Войти',
            onPressed: () => _signIn(context),
          ),
        ],
      ),
    );
  }

  void _signIn(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      BlocProvider.of<SignInBloc>(context).add(
        SignInLogEvent(
          login: _signInController.text,
          password: _passwordController.text,
        ),
      );
    }
  }

  void _onPasswordVisibility() {
    setState(() {
      _visibility = !_visibility;
    });
  }
}
