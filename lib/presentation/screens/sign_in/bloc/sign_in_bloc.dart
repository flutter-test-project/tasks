import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/domain/entities/user.dart';
import 'package:mytasks/domain/usecase/get_user_usecase.dart';



part 'sign_in_event.dart';

part 'sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final GetUserUseCase _getUserUseCase;

  SignInBloc(this._getUserUseCase) : super(SignInInit()) {
    on<SignInLogEvent>(_onSignIn);
  }

  Future<void> _onSignIn(
      SignInLogEvent event, Emitter<SignInState> emit) async {
    try {
      emit(SignInLoadingState());
      final User? user =
          await _getUserUseCase.call(SignInData(event.login, event.password));
      if (user == null) {
        emit(const SignInErrorState('Пользователь не найден'));
      } else {
        emit(SignInLoadedState(user));
      }
    } catch (error) {
      emit(SignInErrorState(error.toString()));
    }
  }
}
