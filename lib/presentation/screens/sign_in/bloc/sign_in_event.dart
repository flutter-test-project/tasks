part of 'sign_in_bloc.dart';

@immutable
abstract class SignInEvent extends Equatable {
  const SignInEvent();

  @override
  List<Object?> get props => [];
}

class SignInLogEvent extends SignInEvent {
  final String login;
  final String password;

  const SignInLogEvent({
    required this.login,
    required this.password,
  });

  @override
  List<Object> get props => [login, password];
}
