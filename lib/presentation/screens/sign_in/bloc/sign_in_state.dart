part of 'sign_in_bloc.dart';

@immutable
sealed class SignInState extends Equatable {
  const SignInState();

  @override
  List<Object> get props => [];
}

class SignInInit extends SignInState {}

class SignInLoadingState extends SignInState {}

class SignInLoadedState extends SignInState {
  final User user;

  const SignInLoadedState(this.user);
}

class SignInErrorState extends SignInState {
  final String errorText;

  const SignInErrorState(this.errorText);
}
