import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/domain/usecase/task_change_status_usecase.dart';

part 'task_edit_event.dart';

part 'task_edit_state.dart';

class TaskEditBloc extends Bloc<TaskEditEvent, TaskEditState> {
  final TaskChangeStatusUseCase _taskChangeStatusUseCase;

  TaskEditBloc(this._taskChangeStatusUseCase) : super(TaskEditInitState()) {
    on<TaskEditStatusEvent>(_editStatusTask);
  }

  Future<void> _editStatusTask(
    TaskEditStatusEvent event,
    Emitter<TaskEditState> emit,
  ) async {
    try {
      emit(TaskEditLoadingState());
      await _taskChangeStatusUseCase.call(
        TaskChangeStatusData(
          taskId: event.taskId,
          statusId: event.newStatus.id,
        ),
      );
      emit(
        EditStatusSuccessState(
          taskId: event.taskId,
          taskStatus: event.newStatus,
        ),
      );
    } catch (error) {
      emit(EditStatusErrorState());
    }
  }
}
