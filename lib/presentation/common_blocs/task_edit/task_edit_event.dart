part of 'task_edit_bloc.dart';

@immutable
abstract class TaskEditEvent extends Equatable {
  const TaskEditEvent();

  @override
  List<Object?> get props => [];
}

class TaskEditStatusEvent extends TaskEditEvent {
  final TaskStatus newStatus;
  final String taskId;

  const TaskEditStatusEvent({
    required this.newStatus,
    required this.taskId,
  });

  @override
  List<Object> get props => [newStatus, taskId];
}
