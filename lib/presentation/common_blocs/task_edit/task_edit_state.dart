part of 'task_edit_bloc.dart';

@immutable
sealed class TaskEditState extends Equatable {
  const TaskEditState();

  @override
  List<Object?> get props => [];
}

class TaskEditInitState extends TaskEditState {}

class TaskEditLoadingState extends TaskEditState {}

class EditStatusErrorState extends TaskEditState {}

class EditStatusSuccessState extends TaskEditState {
  final String taskId;
  final TaskStatus taskStatus;

  const EditStatusSuccessState({
    required this.taskId,
    required this.taskStatus,
  });
}
