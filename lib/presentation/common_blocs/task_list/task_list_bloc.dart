import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytasks/domain/entities/task.dart';
import 'package:mytasks/domain/entities/task_status.dart';
import 'package:mytasks/domain/usecase/base_usecase.dart';
import 'package:mytasks/domain/usecase/get_tasks_usecase.dart';

part 'task_list_event.dart';

part 'task_list_state.dart';

class TaskListBloc extends Bloc<TaskListEvent, TaskListState> {
  final GetTasksUseCase _getTasksUseCase;

  TaskListBloc(this._getTasksUseCase) : super(TaskListInitState()) {
    on<TaskListFetchedEvent>(_onFetched);
    on<TaskListRefreshEvent>(_onRefresh);
    on<AddTaskInListEvent>(_addTaskInList);
    on<DeleteTaskEvent>(_deleteTask);
    on<ChangeTaskStatusEvent>(_changeTaskStatus);
  }

  Future<void> _onFetched(
      TaskListFetchedEvent event, Emitter<TaskListState> emit) async {
    try {
      emit(TaskListLoadingState());
      final List<Task> tasks = await _getTasksUseCase.call(NoParams());
      emit(TaskListLoadedState(
        tasks: tasks,
      ));
    } catch (error) {
      emit(TaskListErrorState(error.toString()));
    }
  }

  Future<void> _onRefresh(
      TaskListRefreshEvent event, Emitter<TaskListState> emit) async {
    final oldState = state;
    if (oldState is TaskListLoadedState) {
      try {
        emit(TaskListLoadingState());
        final List<Task> tasks = await _getTasksUseCase.call(NoParams());
        emit(
          TaskListLoadedState(tasks: tasks),
        );
      } catch (error) {
        emit(TaskListErrorState(error.toString()));
        emit(
          TaskListLoadedState(
            tasks: oldState.tasks,
          ),
        );
      }
    }
  }

  Future<void> _addTaskInList(
    AddTaskInListEvent event,
    Emitter<TaskListState> emit,
  ) async {
    final oldState = state;
    if (oldState is TaskListLoadedState) {
      try {
        emit(TaskListLoadedState(
          tasks: [...oldState.tasks, event.task],
        ));
      } catch (error) {
        emit(TaskListErrorState(error.toString()));
        emit(TaskListLoadedState(
          tasks: oldState.tasks,
        ));
      }
    }
  }

  Future<void> _deleteTask(
      DeleteTaskEvent event, Emitter<TaskListState> emit) async {
    final oldState = state;
    if (oldState is TaskListLoadedState) {
      try {
        final List<Task> newTaskList = oldState.tasks
            .where((element) => element.id != event.taskId)
            .toList();

        emit(
          TaskListLoadedState(tasks: newTaskList),
        );
      } catch (error) {
        emit(TaskListErrorState(error.toString()));
        emit(TaskListLoadedState(
          tasks: oldState.tasks,
        ));
      }
    }
  }

  Future<void> _changeTaskStatus(
    ChangeTaskStatusEvent event,
    Emitter<TaskListState> emit,
  ) async {
    final oldState = state;
    if (oldState is TaskListLoadedState) {
      try {
        final index =
            oldState.tasks.indexWhere((element) => element.id == event.taskId);
        if (index != -1) {
          final Task updateTask =
              oldState.tasks[index].copyWith(status: event.taskStatus);
          emit(
            TaskListLoadedState(
              tasks: oldState.tasks.toList()
                ..removeAt(index)
                ..insert(index, updateTask),
            ),
          );
        } else {
          emit(const TaskListErrorState('Не удалось опознать задачу'));
          emit(TaskListLoadedState(tasks: oldState.tasks));
        }
      } catch (error) {
        emit(TaskListErrorState(error.toString()));
        emit(TaskListLoadedState(
          tasks: oldState.tasks,
        ));
      }
    }
  }
}
