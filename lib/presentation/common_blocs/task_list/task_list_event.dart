part of 'task_list_bloc.dart';

@immutable
abstract class TaskListEvent extends Equatable {
  const TaskListEvent();

  @override
  List<Object?> get props => [];
}

class TaskListFetchedEvent extends TaskListEvent {}

class TaskListRefreshEvent extends TaskListEvent {}

class AddTaskInListEvent extends TaskListEvent {
  final Task task;

  const AddTaskInListEvent(this.task);

  @override
  List<Object?> get props => [task];
}

class DeleteTaskEvent extends TaskListEvent {
  final String taskId;

  const DeleteTaskEvent(this.taskId);

  @override
  List<Object?> get props => [taskId];
}

class ChangeTaskStatusEvent extends TaskListEvent {
  final String taskId;
  final TaskStatus taskStatus;

  const ChangeTaskStatusEvent({
    required this.taskId,
    required this.taskStatus,
  });

  @override
  List<Object?> get props => [taskId, taskStatus];
}
