part of 'task_list_bloc.dart';

@immutable
sealed class TaskListState extends Equatable {
  const TaskListState();

  @override
  List<Object?> get props => [];
}

class TaskListInitState extends TaskListState {}

class TaskListLoadingState extends TaskListState {}

class TaskListLoadedState extends TaskListState {
  final List<Task> tasks;

  const TaskListLoadedState({
    required this.tasks,
  });

  @override
  List<Object> get props => [tasks];

  Task? getTask(String taskId) {
    for (var task in tasks) {
      if (task.id == taskId) {
        return task;
      }
    }
    return null;
  }

  int get countTaskStatusToDo {
    return tasks.where((element) => element.status == TaskStatus.todo).length;
  }

  List<Task> get tasksDataSort {
    tasks.sort((a, b) {
      int aDate = a.creationData.microsecondsSinceEpoch;
      int bDate = b.creationData.microsecondsSinceEpoch;
      return bDate.compareTo(aDate);
    });
    return tasks;
  }
}

class TaskListErrorState extends TaskListState {
  final String errorText;

  const TaskListErrorState(this.errorText);
}
