import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mytasks/domain/usecase/task_delete_usecase.dart';

part 'task_delete_event.dart';

part 'task_delete_state.dart';

class TaskDeleteBloc extends Bloc<TaskDeleteEvent, TaskDeleteState> {
  final TaskDeleteUseCase _taskDeleteUseCase;

  TaskDeleteBloc(this._taskDeleteUseCase) : super(TaskDeleteInitState()) {
    on<DeleteEvent>(_delete);
  }

  Future<void> _delete(DeleteEvent event, Emitter<TaskDeleteState> emit) async {
    try {
      await _taskDeleteUseCase.call(event.idTask);
      emit(
        TaskDeleteSuccessState(
          taskId: event.idTask,
        ),
      );
    } catch (error) {
      emit(TaskDeleteErrorState());
    }
  }
}
