part of 'task_delete_bloc.dart';

@immutable
abstract class TaskDeleteEvent extends Equatable {
  const TaskDeleteEvent();

  @override
  List<Object?> get props => [];
}

class DeleteEvent extends TaskDeleteEvent{
  final String idTask;

  const DeleteEvent(this.idTask);
}