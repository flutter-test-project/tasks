part of 'task_delete_bloc.dart';

@immutable
sealed class TaskDeleteState extends Equatable {
  const TaskDeleteState();

  @override
  List<Object?> get props => [];
}

class TaskDeleteInitState extends TaskDeleteState {}

class TaskDeleteErrorState extends TaskDeleteState {}

class TaskDeleteSuccessState extends TaskDeleteState {
  final String taskId;

  const TaskDeleteSuccessState({required this.taskId});
}
