# mytasks

It's a test project for create tasks.

## Technologies used
* [Dart](https://dart.dev)
* [Flutter](https://flutter.dev)
* [BLOC](https://bloclibrary.dev)
* [Firebase](https://firebase.google.com)

